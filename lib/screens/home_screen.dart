import 'package:flutter/material.dart';
import '../services/api_service.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> _users = [];
  String? _selectedUser; // Permitir null

  int _recognitions = 0;

  @override
  void initState() {
    super.initState();
    _loadUsers();
    _loadRecognitions();
  }

  void _loadUsers() async {
    try {
      var users = await ApiService.getUsers();
      setState(() {
        _users = users;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void _loadRecognitions() async {
    try {
      var recognitions = await ApiService.getRecognitions();
      setState(() {
        _recognitions = recognitions;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void _sendCongratulation() async {
    if (_selectedUser == null) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Error'),
          content: Text('Por favor, selecciona un usuario receptor.'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
      return;
    }

    try {
      await ApiService.sendCongratulation(_selectedUser!);
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Éxito'),
          content: Text('Felicitación enviada con éxito'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Enviar Felicitaciones'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            DropdownButton<String>(
              hint: Text('Selecciona un usuario'),
              value: _selectedUser,
              onChanged: (String? newValue) { // Permitir null
                setState(() {
                  _selectedUser = newValue;
                });
              },
              items: _users.map((user) {
                return DropdownMenuItem<String>(
                  value: user,
                  child: Text(user),
                );
              }).toList(),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _sendCongratulation,
              child: Text('Enviar'),
            ),
            SizedBox(height: 20),
            Text('Tienes $_recognitions Reconocimientos'),
          ],
        ),
      ),
    );
  }
}
