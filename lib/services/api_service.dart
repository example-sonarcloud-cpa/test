import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiService {
  static Future<List<String>> getUsers() async {
    final response = await http.get(
      Uri.parse('https://appmobile-api.crisops.com/usuarios/'),
      headers: {'accept': 'application/json'},
    );

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      return data.where((user) => user['Estado'] == 'A').map((user) => user['NombreUsuario'].toString()).toList();
    } else {
      throw Exception('Error al obtener los usuarios');
    }
  }

  static Future<int> getRecognitions() async {
    final response = await http.get(
      Uri.parse('https://appmobile-api.crisops.com/bandas/'),
      headers: {'accept': 'application/json'},
    );

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      return data.where((banda) => banda['IDUsuarioEnvia'] == 1).length;
    } else {
      throw Exception('Error al obtener los reconocimientos');
    }
  }

  static Future<void> sendCongratulation(String receiver) async {
    final response = await http.post(
      Uri.parse('https://appmobile-api.crisops.com/bandas/'),
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        'IDUsuarioRecibe': receiver,
        'IDUsuarioEnvia': 1,
      }),
    );

    if (response.statusCode != 200) {
      throw Exception('Error al enviar la felicitación');
    }
  }
}
