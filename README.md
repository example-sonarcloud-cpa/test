# Flutter App

## Descripción

Esta es una aplicación Flutter para enviar felicitaciones a usuarios. La aplicación tiene un sistema de autenticación básico y una funcionalidad para enviar felicitaciones y ver el número de reconocimientos recibidos.

## Requisitos

- Flutter SDK
- Android Studio
- Android SDK

## Configuración del Ambiente

1. Instalar Flutter SDK.
2. Instalar Android Studio y configurar Android SDK.
3. Clonar este repositorio.
4. Ejecutar `flutter pub get` para instalar las dependencias.

## Construcción del APK

1. Navegar al directorio del proyecto.
2. Ejecutar `flutter build apk --release`.
3. Encontrar el APK en `build/app/outputs/flutter-apk/app-release.apk`.

## Ejecución

1. Conectar un dispositivo Android o iniciar un emulador.
2. Ejecutar `flutter run` para ejecutar la aplicación en modo debug.
